def method_4(countries: [str, list]):
    """This function receives a string with a country or a list of country strings. 
    It plots the total of the "_consumption" column of the specified countries. 
    Args:
        s (string): country string
    Returns:
        nothing, the function only plots the graph.
    """
    # To dos: 
    # - how to import the df?
    # - what happens if you call the method with an int? or something else?
    
    
    # Converting countries to a list of strings, if it is a str
    if isinstance(countries, str):
        countries = countries.split("delimiter")
    
    # Create the total consumption column and aggregate data by country
    df_aggregate = df.groupby(by = "country").sum().reset_index()
    all_columns = list(df_aggregate.columns)
    relevant_columns = list(filter(lambda x: '_consumption' in x, all_columns))
    df_aggregate['total_consumption'] = df_aggregate[relevant_columns].sum(axis = 1, skipna = True)
    
    # Select the countries to be plotted
    df_aggregate_selected = df_aggregate[df_aggregate["country"].isin(countries)]
    
    df_aggregate_selected.plot(x = "country", y = "total_consumption", kind = 'bar', figsize = (15,5), 
                           title = "Countries by total consumption", grid = True, xlabel = "Countries",
                           ylabel = "Consumption in xx", legend = False) # Add colering and seaborn style.
    
def method_5(countries: str):
    """This function receives a string with a country or a list of country strings. 
    It plots the "gdp" column of the specified countries over the years. 
    Args:
        s (string): country string
    Returns:
        nothing, the function only plots the graph.
    """
    
    import matplotlib
    matplotlib.style.use("seaborn")

    # Converting countries to a list of strings, if it is a str
    if isinstance(countries, str):
        countries = countries.split("delimiter")
    
    # Select the countries to be plotted
    df_gdp_selected = df[df["country"].isin(countries)]

    df_gdp_selected = df_gdp_selected[["country", "year", "gdp"]]
    df_gdp_selected = df_gdp_selected.pivot(index = "year", columns = "country", values = "gdp")

    
    # Plot the countries
    df_gdp_selected.plot(figsize = (15,5), title = "Countries by GDP over years", grid = True, 
                         xlabel = "Years", ylabel = "GDP", legend = True)

